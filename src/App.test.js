import {render, screen} from '@testing-library/react';
import {BrowserRouter} from "react-router-dom";
import App from './App'
import React from "react";
import UserProvider from './contexts/UserContext.tsx';
import FormLogin from "./components/forms/FormLogin.tsx";
import PageSelector from "./components/PageSelector.tsx";
import PlaylistItem from './components/items/PlaylistItem.tsx';
import FormPlaylistItem from './components/forms/FormPlaylistItem.tsx';

it('Renders without crashing', () => {
    render(<App/>);
});

it('Playlist component renders without crashing', () => {
    renderPlaylistItem();
});

it('FormPlaylistItem component renders without crashing', () => {
    const playlistItem = getPlaylistItemTest();
    render(<FormPlaylistItem albumId={1} playlistItemId={1} playlistItem={playlistItem}/>)
    render(<FormPlaylistItem albumId={1} playlistItemId={0}/>)
    render(<FormPlaylistItem albumId={0}/>)
});

it('PageSelector component renders without crashing', () => {
    const currentPage = 1;
    const paginationData = {
        'links': {'first': null, 'last': null, 'prev:': null, 'next': null},
        'meta': {'current_page': 1, 'last_page': 1, 'links': []}
    }
    render(<PageSelector/>)
    render(<PageSelector currentPage={currentPage} paginationData={paginationData.meta} changePage={jest.fn()}/>)
});

it('FormLogin component renders without crashing', () => {
    render(<FormLogin/>)
});

test('action buttons not present if not logged', () => {
    renderPlaylistItem(null);
    const buttons = screen.queryAllByRole('button');

    expect(buttons).toHaveLength(3)
});

test('action buttons present if logged', () => {
    renderPlaylistItem('12345678');
    const buttons = screen.queryAllByRole('button');

    expect(buttons).toHaveLength(5)
});

function renderPlaylistItem(token = null) {
    const context = {userProfile: null, updateUserProfile: ()=>{}, removeUserProfile: ()=>{}};
    const playlistItem = getPlaylistItemTest()
    render(
        <UserProvider>
            <BrowserRouter>
                <PlaylistItem key={playlistItem.id} id={playlistItem.id} item={playlistItem}
                editButtonCallback={jest.fn()} deleteButtonCallback={jest.fn()}
                token={token} owned={token !== null}/>
            </BrowserRouter>
        </UserProvider>, {context});
}

function getPlaylistItemTest(isNull = false) {
    return {
        id: 1,
        isPrivate: false,
        note: -1,
        comment: '',
        listeningDate: null,
        listened: false,
        user: {
            id: 1,
            name: 'test'
        },
        album: {
            id: 1,
            artist: 'Unknown',
            title: 'Untitled',
            url: 'http://example.com',
            bandcampItemId: null,
            bandcampItemType: null,
            genre: ['HNW', 'Black Metal'],
        }
    };
}
