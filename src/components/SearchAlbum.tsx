import React from 'react';
import { useState } from 'react';
import AlbumEdition from './forms/AlbumEdition';
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Dialog from "@mui/material/Dialog";
import SearchIcon from "@mui/icons-material/Search";
import {Checkbox, FormControlLabel, IconButton, TextField} from "@mui/material";
import { IAlbum } from '../@types/album';
import { apiGetAlbums } from '../services/AlbumService';
import { ModifierKey, useKeyPress } from '../hooks/KeyPress';

type Props = {
    callback: Function,
    token: string,
    setRefresh: Function,
    withCreation: boolean,
}

export default function SearchAlbum({callback, token, setRefresh, withCreation=false}: Props) {
    const [open, setOpen] = useState(true);
    const [listened, setListened] = useState(false);
    const [search, setSearch] = useState('');
    const [albumId, setAlbumId] = useState(0);
    const [isAdding, setIsAdding] = useState(false);
    const [albumList, setAlbumList] = useState<IAlbum[]>([]);

    const onKeyPress = (e: any) => {
        e.preventDefault();
        if (e.key === "Enter") {
            handleSearch();
        }
    }

    const onClickRow = (id: number) => {
        let selectedElement = document.getElementById("album-"+id)
        if (selectedElement === null) {
            return;
        }

        if (selectedElement.classList.contains("selected")) {
            selectedElement.classList.remove("selected");
            setAlbumId(0);
        } else {
            let elements = document.querySelectorAll(".search-table tr.selected");
            elements.forEach(element => {
                if(element.id !== `album${id}`) {
                    element.classList.remove("selected");
                }
            });
            selectedElement.classList.add("selected");
            setAlbumId(id);
        }
    }

    const onDoubleClickRow = (e: any, id: number) => {
        setAlbumId(id);
        handleSelect(e, id)
    }

    const handleSearch = async () => {
        if (search !== "") {
            apiGetAlbums(`&search=${search}`, token, 1).then((data) => {if(data){setAlbumList(data.albums)}});
        } else {
            setAlbumList([]);
        }
    }

    const handleCreation = (e: any) => {
        e.preventDefault();
        setOpen(false);
        setIsAdding(true);
    }
    useKeyPress(["c"], handleCreation, ModifierKey.Alt);

    const handleClose = (e: any) => {
        e.preventDefault();
        callback();
        setOpen(false);
    }

    const handleSelect = (e: any, id: number=0) => {
        e.preventDefault();
        if (id === 0) {
            callback(albumId, listened);
        } else {
            callback(id, listened);
        }
        setOpen(false);
    };

    return (
        <>
            <Dialog open={open} onClose={handleClose}>
                <DialogTitle>Search album</DialogTitle>
                <DialogContent sx={{padding: "5%", minWidth: "90%"}}>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="search-text"
                        name="search-text"
                        label={"Search"}
                        sx={{"width": "85%"}}
                        variant="standard"
                        onChange={e => setSearch(e.target.value)}
                        onKeyUp={onKeyPress}
                    />
                    <IconButton onClick={handleSearch}
                                style={{"verticalAlign": "bottom"}}><SearchIcon
                        fontSize="small"/></IconButton>

                    <p hidden={albumList.length !== 0} className='text-center text-bold'>No album</p>
                    <table hidden={albumList.length === 0} className='search-table'>
                        <thead>
                            <tr>
                                <th>Artist</th>
                                <th>Title</th>
                                <th>Genre</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {albumList.map((album) => (
                                <tr key={album.id}
                                    id={"album-"+album.id} 
                                    onClick={() => onClickRow(album.id)}
                                    onDoubleClick={(e) => onDoubleClickRow(e, album.id)}
                                    >
                                    <td>{album.artist}</td>
                                    <td>{album.title}</td>
                                    <td>{album.genre.join(', ')}</td>
                                    {/* Create a button for the link (easier to style) */}
                                    <td><a target='_blank' rel='noreferrer' href={album.url}>Link</a></td>
                                </tr>
                            ))}
                        </tbody>
                    </table>

                    <p><FormControlLabel
                        control={<Checkbox
                            id="listened"
                            name="listened"
                            checked={listened}
                            onChange={e => setListened(e.target.checked)}
                        />} label="Listened"/></p>
                </DialogContent>
                <DialogActions>
                    {
                        withCreation ?
                        <button className='action-button' onClick={handleCreation}>Create</button> :
                        ""
                    }
                    
                    <button className="action-button" onClick={handleClose}>Cancel</button>
                    <button id="select-button" className="action-button" onClick={handleSelect}>Select</button>
                </DialogActions>
            </Dialog>

            <AlbumEdition
                token={token}
                setRefresh={setRefresh}
                isEditing={false}
                setIsEditing={() => {}}
                isAdding={isAdding}
                setIsAdding={setIsAdding}
                albumInEdition={null}
                setAlbumInEdition={() => {}}
                callback={callback}
                addingPlaylistItem={true}
            />
        </>
    );
}