import { IAlbum } from "./album";
import { IPaginationDataLinks, IPaginationDataMeta } from "./paginationData";
import { IUser } from "./user";

export interface IPlaylistItem {
    id: number,
    note: number,
    listened: boolean,
    listeningDate: Date,
    comment: string,
    isPrivate: boolean,
    favourite: boolean,
    user: IUser,
    album: IAlbum
}

export interface IPlaylistItemPayload {
    albumId: number,
    note: number,
    listened: boolean,
    listeningDate: Date | null,
    isPrivate: boolean,
    favourite: boolean,
    comment: string | null
}

export interface IPlaylistItemPaginatedCollection {
    items: IPlaylistItem[],
    links: IPaginationDataLinks | null,
    meta: IPaginationDataMeta | null
}

export const getPayloadFromIPlaylistItem = (playlistItem: IPlaylistItem): IPlaylistItemPayload => {
    return {
        albumId: playlistItem.album.id,
        note: playlistItem.note,
        listened: playlistItem.listened,
        listeningDate: playlistItem.listeningDate,
        isPrivate: playlistItem.isPrivate,
        favourite: playlistItem.favourite,
        comment: playlistItem.comment,
    }
}