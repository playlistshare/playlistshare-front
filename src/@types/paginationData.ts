export interface IPaginationDataLinks {
    first: string,
    last: string,
    prev: string | null,
    next: string | null,
}

export interface IPaginationDataMeta {
    current_page: number,
    last_page: number,
    links: [{
        url: string,
        label: string,
        active: boolean,
    }]
}