export interface IBandcampDetail {
    type: string,
    id: string,
    artist: string,
    title: string,
}