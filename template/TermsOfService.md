## Terms of Service for [PlaylistShare]

### Effective Date: 2024-01-12

### 1. Acceptance of Terms

By accessing or using the [PlaylistShare] ("the App"), you agree
to comply with and be bound by these Terms of Service. If you do not agree with
these terms, please do not use the App.

### 2. Changes to Terms

The administrator of this instance of the App reserves the right to modify, amend, or update these
Terms of Service at any time. Changes will be effective immediately upon posting. Your
continued use of the App after any such modifications will constitute your acknoledgment
of the modified Terms of Service and agreement to abide and be bound by the modified terms.

### 3. User Registration

To use certain features of the App, you may be required to register for an account.
You agree to provide accurate, current and complete information during the registration
process and to update such information to keep it accurate, current, and complete.

### 4. Use of the App

a. You agree to use the App solely for personal, non-commercial purposes.

b. You may not use the App to transmit any unauthorized advertising, promotional
materials, junk mail, spam, chain letters, pyramid schemes, or any other form of solocitation.

c. You may not use the App in any manner that could disable, overburden, damage, or
impair the functionality of the App or interfere with any other party's use of the
App.

### 5. Music link sharing

a. The App enables users to share links to music content from various platforms.
Users are solely responsible for the content of the links they share.

b. You acknowledge and agree that the administrator of the instance does not control, endorse, 
or take responsibility for the content linked through the App. 
The administrator of the instance is not responsible for the accuracy, appropriateness, or legality
of the linked content.

c. The administrator of this instance reserves the right to remove any shared music links that,
in its sole discretion, violate these Term of Service or are deemed inappropriate or objectionable.

d. Users are prohibited from sharing music links that infringe upon the intellectual property 
rights of others. The administrator of this instance will respond to notices of alleged copyright
infringement in accordance with applicable laws.

### 6. User Comments

a. The app encourages users to engage in positive and respectful interactions. User may have the 
opportunity to post comments, share feedback, or otherwise interact with the App and its community.

b. Respectful Conduct: Users are expected to communicate with each other and the administrator in
a respectful and considerate manner. Any communication, including comments, should be constructive,
supportive, and free from offensive language.

c. Prohibited Content: Hateful, discriminatory, defamatory, or otherwise inappropriate comments are
stricly prohibited. Users must refrain from posting content that promotes violence, harassment, or any
form of harm towards individuals or groups based on attributes such as race, ethnicity, religion, gender,
sexual orientation, disability, or any other protected characteristic.

d. Moderation: The administrator reserves the right to monitor, review, and remove any user comments
that violate these terms or are deemed inappropriate. Users engaging in repeated violations may face
account suspension or termination at our discretion.

e. User Responsibility: Users are solely responsible for the content of their comments. By posting
comments on the App, users affirm that they have the necessary rights to share the content and that
their comments comply with these Terms of Service.

### 7. Privacy Policy

Your use of the App is also governed by our [Privacy Policy](/privacy-policy), which is incorporated by reference. 
Please review the Privacy Policy to understand our practices regarding the collection and use of your personal information.

### 8. Termination

The administrator of this instance of the App reserves the right
to terminate you account and access to the App, with or without
cause, at any time and without notice.

### 9. Disclaimer of Warranties

The App is provided on an "as-is" and "as-available" basis. 
The administrator of this instance makes no representations or warranties of any kind, 
whether express or implied, regarding the App or its content.

**Please review these Terms of Service carefully.**
**By using the App, you agree to be bound by these terms.**
**If you do not agree to these terms, please do not use the App.**
