FROM node:14-alpine
WORKDIR /app
COPY ./ /app/

#Environment variable
ENV REACT_APP_API_URL="http://localhost:8000/api/v1"
ENV REACT_APP_DEBUG=0
ENV REACT_APP_TITLE="Playlist Share"
ENV REACT_APP_MAX_ITEM_PER_PAGE=10

RUN npm install
RUN npm run build
RUN npm install -g serve

EXPOSE 3000
CMD serve -s build
